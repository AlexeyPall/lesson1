

import numpy as np


class DTW:
    """ """

    def __init__(self, signal1, signal2):
        """ """

        ''' Fill distance matrix using Euclidean distance '''
        self.distance_matrix = DTW.fill_distance_matrix(signal1, signal2)

        print(self.distance_matrix)
        DTW.print_arrows(self.distance_matrix)

    @staticmethod
    def fill_distance_matrix(signal1, signal2):
        """ """

        ''' Init 2D matrix to store distance between signals '''
        distance_matrix = np.zeros([len(signal1) + 1, len(signal2) + 1])

        distance_matrix[:, 0] = float('inf')
        distance_matrix[0, :] = float('inf')
        distance_matrix[0, 0] = 0

        for _sig1_idx in range(len(signal1)):
            for _sig2_idx in range(len(signal2)):

                ''' Calculate  Euclidean distance between two vectors '''
                cost = DTW.euclidean_distance(signal1[_sig1_idx], signal2[_sig2_idx])

                min_dst = min(distance_matrix[_sig1_idx, _sig2_idx + 1],
                              distance_matrix[_sig1_idx + 1, _sig2_idx],
                              distance_matrix[_sig1_idx, _sig2_idx])

                distance_matrix[_sig1_idx + 1, _sig2_idx + 1] = cost + min_dst

        return distance_matrix

    @staticmethod
    def euclidean_distance(vec1, vec2):
        return np.sqrt(np.sum(np.power(vec1 - vec2, 2)))

    @staticmethod
    def print_arrows(dist_matrix):
        """ """

        result_arrows = []

        for n in range(1, len(dist_matrix)):

            str_arrows = ''

            for m in range(1, len(dist_matrix[n])):

                tmp_arr = [dist_matrix[n-1][m-1],
                           dist_matrix[n][m - 1],
                           dist_matrix[n - 1][m]]

                # print(tmp_arr)
                # print(tmp_arr.index(min(tmp_arr)))

                if tmp_arr.index(min(tmp_arr)) == 0:
                    ''' Diagonal '''
                    str_arrows += u'\u2196 '
                elif tmp_arr.index(min(tmp_arr)) == 2:
                    ''' Top '''
                    str_arrows += u'\u2191 '
                else:
                    ''' Left '''
                    str_arrows += u'\u2190 '

            print(str_arrows)
            # result_arrows = [str_arrows, ] + result_arrows

        # [print(_str) for _str in result_arrows]


def init_signal(init_values):

    signal = np.empty(len(init_values))

    for _idx in range(len(init_values)):
        signal[_idx] = init_values[_idx]

    return signal


# TEST 1
def test1():
    sig1 = init_signal((0, 1, 1, 2, 0))
    sig2 = init_signal((0, 0, 0.9, 0.9, 1.2, 2.1, 0))

    DTW(sig1, sig2)

# TEST 2

# da1 = init_signal((0, 1, 2, 0))
# net1 = init_signal((0, 1, 0))
#
# record1 = init_signal((0, 0.9, 1.3, 0))

# DTW(record1, da1)
# DTW(record1, net1)

# print('\n\n')
# print(u'\u2190', u'\u2191', u'\u2192', u'\u2193')
# print(u'\u2196', u'\u2197', u'\u2198', u'\u2199')
