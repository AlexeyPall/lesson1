#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys

import numpy as np

import FtrFile


def euclidean_distance(vec1, vec2):
    return np.sqrt(np.sum(np.power(vec1 - vec2, 2)))


#########################################
# State, Graph, etc...
#########################################

class State:
    def __init__(self, ftr, idx):  # idx is for debug purposes
        self.ftr = ftr
        self.word = None
        self.isFinal = False
        self.nextStates = []
        self.idx = idx


def print_state(state):
    nextStatesIdxs = [s.idx for s in state.nextStates]
    print("State: idx={} word={} isFinal={} nextStatesIdxs={} ftr={} ".format(
        state.idx, state.word, state.isFinal, nextStatesIdxs, state.ftr))


def load_graph(rxfilename):
    startState = State(None, 0)
    graph = [startState, ]
    stateIdx = 1
    for word, features in FtrFile.FtrDirectoryReader(rxfilename):
        prevState = startState
        for frame in range(features.nSamples):
            state = State(features.readvec(), stateIdx)
            state.nextStates.append(state)  # add loop
            prevState.nextStates.append(state)
            prevState = state
            graph.append(state)
            stateIdx += 1
        if state:
            state.word = word
            state.isFinal = True
    return graph


def check_graph(graph):
    assert len(graph) > 0, "graph is empty."
    assert graph[0].ftr is None \
        and graph[0].word is None \
        and not graph[0].isFinal, "broken start state in graph."
    idx = 0
    for state in graph:
        assert state.idx == idx
        idx += 1
        assert (state.isFinal and state.word is not None) \
            or (not state.isFinal and state.word is None)


def print_graph(graph):
    print("*** DEBUG. GRAPH ***")
    np.set_printoptions(formatter={'float': '{: 0.1f}'.format})
    for state in graph:
        print_state(state)
    print("*** END DEBUG. GRAPH ***")


#########################################
# Token
#########################################

class Token:
    def __init__(self, state, dist=0.0, sentence=""):
        self.state = state
        self.dist = dist
        self.sentence = sentence


def print_token(token):
    print("Token on state #{} dist={} sentence={}".format(token.state.idx,
                                                          token.dist,
                                                          token.sentence))


def print_tokens(tokens):
    print("*** DEBUG. TOKENS LIST ***")
    for token in tokens:
        print_token(token)
    print("*** END DEBUG. TOKENS LIST ***")


def print_result_tokens_to_file(final_tokens, winner_token, file_wit_results):
    """ Print to file result all active tokens and the winner token """

    res_out_file = open(file_wit_results, 'w')

    ''' Write all active tokens to file '''
    for _token in final_tokens:
        res_out_file.write('State: ' + str(_token.state.idx) +
                           ' dist: ' + str(_token.dist) + '\n')

    ''' Write the Winner token '''
    res_out_file.write('Winner token :\n')
    res_out_file.write('State: ' + str(winner_token.state.idx) +
                       ' dist: ' + str(winner_token.dist))

#########################################
# Decoder
#########################################

def recognize(filename, features, graph, file_wit_results):
    print("Recognizing file '{}', samples={}".format(filename,
                                                     features.nSamples))

    startState = graph[0]
    nextTokens = [Token(startState), ]

    for _idx in range(features.nSamples):

        ''' Get next feature vector '''
        _ftr = features.readvec()

        ''' Set nextTokens as active tokens. Clear nextTokens  '''
        activeTokens = nextTokens
        nextTokens = []

        print('#' * 30)
        # print_tokens(activeTokens)
        print('Nsampl: ', features.nSamples, ' cur: ', _idx, ' len: ', len(activeTokens))
        print('#' * 30)

        for _token in activeTokens:

            ''' Generate all possible new tokens '''
            for _next_state in _token.state.nextStates:

                ''' Calc distance between state and new ftr '''
                new_dist = _token.dist + euclidean_distance(_ftr, _next_state.ftr)

                ''' Create a new token '''
                new_token = Token(_next_state, new_dist)

                ''' Add new token to next evaluated token's list '''
                nextTokens.append(new_token)

    ''' Get result '''
    print_tokens(nextTokens)

    # print(len(nextTokens))

    final_tokens = []
    for _token in nextTokens:
        if _token.state.isFinal:
            final_tokens.append(_token)

    distance = [_token.dist for _token in final_tokens]

    print('---------- Winner ----------')
    winner_token = final_tokens[distance.index(min(distance))]
    print_token(winner_token)

    print_result_tokens_to_file(final_tokens, winner_token, file_wit_results)

#########################################
# Main
#########################################

BASE_PATH = '/home/alex/projects/pycharm/ASRlesson020'

if __name__ == "__main__":

    etalons = 'ark,t:data/da1_net1.txtftr'
    records = "ark,t:data/record.txtftr"

    # etalons = 'ark,t:' + BASE_PATH + '/data/da_net.txtftr'
    # records = 'ark,t:' + BASE_PATH + '/data/record.txtftr'

    graph = load_graph(etalons)
    check_graph(graph)
    print_graph(graph)

    for filename, features in FtrFile.FtrDirectoryReader(records):
        recognize(filename, features, graph, 'result.txt')
